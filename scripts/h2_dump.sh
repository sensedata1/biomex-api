#!/bin/bash
#
#

URL="jdbc:h2:file:~/biomexapidb;AUTO_SERVER=TRUE"
USER=sa
PASSWORD=''
DUMP_FILE=$(date +'h2-dump-%Y-%M-%d-%H:%m:%S.sql')

java \
    org.h2.tools.Script \
    -url "${URL}" \
    -user "${USER}" \
    -password "${PASSWORD}" \
    -script "${DUMP_FILE}"

echo "Humpty Dumpty: $DUMP_FILE"
