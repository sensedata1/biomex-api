#!/bin/bash
#
#

URL="jdbc:h2:file:~/biomexapidb;AUTO_SERVER=TRUE"
USER=sa
PASSWORD=''
SCRIPT=${1:?Usage $0 <SQL script>}

java \
    org.h2.tools.RunScript \
    -url "${URL}" \
    -user "${USER}" \
    -password "${PASSWORD}" \
    -script "${SCRIPT}"
