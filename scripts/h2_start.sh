#!/bin/bash
#
#

URL="jdbc:h2:file:~/biomexapidb;AUTO_SERVER=TRUE"
USER=sa
PASSWORD=''

java \
    org.h2.tools.Console \
    -url "${URL}" \
    -user "${USER}" \
    -password "${PASSWORD}"
