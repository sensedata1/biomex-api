package io.biomex.biomex

import io.biomex.biomex.model.BidAskEntity
import io.biomex.biomex.repository.BidAskRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = arrayOf(BidAskRepository::class))
@EntityScan(basePackageClasses = arrayOf(BidAskEntity::class))
@EnableTransactionManagement
internal class InternalBidAskConfig