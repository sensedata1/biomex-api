package io.biomex.biomex

import io.biomex.biomex.model.UserProfileEntity
import io.biomex.biomex.repository.UserProfileRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = [UserProfileRepository::class])
@EntityScan(basePackageClasses = [UserProfileEntity::class])
@EnableTransactionManagement
internal class InternalUserProfileConfig