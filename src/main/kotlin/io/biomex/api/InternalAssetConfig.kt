package io.biomex.biomex

import io.biomex.biomex.model.AssetEntity
import io.biomex.biomex.repository.AssetRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = arrayOf(AssetRepository::class))
@EntityScan(basePackageClasses = arrayOf(AssetEntity::class))
@EnableTransactionManagement
internal class InternalAssetConfig