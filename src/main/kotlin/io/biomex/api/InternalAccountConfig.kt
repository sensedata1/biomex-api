package io.biomex.biomex

import io.biomex.biomex.model.AccountEntity
import io.biomex.biomex.repository.AccountRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = [AccountRepository::class])
@EntityScan(basePackageClasses = [AccountEntity::class])
@EnableTransactionManagement
internal class InternalAccountConfig