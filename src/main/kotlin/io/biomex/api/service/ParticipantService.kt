package io.biomex.biomex.service

import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.ParticipantDto
import io.biomex.biomex.service.dto.UpdateParticipantDto

interface ParticipantService {

    fun findByParticipantId(participantId: Long): ParticipantDto?

    fun findAllParticipants(): List<ParticipantDto>

    fun addParticipant(participant: CreateParticipantDto): ParticipantDto

    fun updateParticipant(id: Long?, participant: UpdateParticipantDto): ParticipantDto?

    fun deleteParticipant(id: Long?)

}

