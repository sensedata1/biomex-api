package io.biomex.biomex.service

import io.biomex.biomex.service.dto.AddressDto
import io.biomex.biomex.service.dto.CreateAddressDto
import io.biomex.biomex.service.dto.UpdateAddressDto

interface AddressService {

    fun findByAddressId(addressId: Long): AddressDto?

    fun findAllAddresses(): List<AddressDto>

    fun addAddress(address: CreateAddressDto): AddressDto

    fun updateAddress(id: Long?, address: UpdateAddressDto): AddressDto?

    fun deleteAddress(id: Long?)

}