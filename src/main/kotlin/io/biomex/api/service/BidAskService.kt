package io.biomex.biomex.service

import io.biomex.biomex.service.dto.BidAskDto
import io.biomex.biomex.service.dto.CreateBidAskDto
import io.biomex.biomex.service.dto.UpdateBidAskDto

interface BidAskService {

    fun findByBidAskId(bidAskId: Long): BidAskDto?

    fun findAllBidAsks(): List<BidAskDto>

    fun addBidAsk(bidAsk: CreateBidAskDto): BidAskDto

    fun updateBidAsk(id: Long?, bidAsk: UpdateBidAskDto): BidAskDto?

    fun deleteBidAsk(id: Long?)
}