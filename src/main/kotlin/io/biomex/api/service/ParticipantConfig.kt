package io.biomex.biomex.service

import io.biomex.biomex.InternalParticipantConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalParticipantConfig::class])
class ParticipantConfig
