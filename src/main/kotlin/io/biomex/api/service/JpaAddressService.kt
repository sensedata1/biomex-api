package io.biomex.biomex.service

import io.biomex.biomex.model.AddressEntity
import io.biomex.biomex.repository.AddressRepository
import io.biomex.biomex.service.dto.AddressDto
import io.biomex.biomex.service.dto.CreateAddressDto
import io.biomex.biomex.service.dto.UpdateAddressDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("addressService")
@Transactional
internal class JpaAddressService(val accountRepo: AddressRepository) : AddressService {

    val log = LoggerFactory.getLogger("addressService")

    override fun findByAddressId(addressId: Long): AddressDto? {
        log.debug("Find Address: {}", addressId)
        return accountRepo.findOne(addressId)?.toDto()
    }

    override fun findAllAddresses(): List<AddressDto> {
        log.debug("Find all Addresses")
        return accountRepo.findAll().map { it.toDto() }
    }

    override fun updateAddress(id: Long?, address: UpdateAddressDto): AddressDto? {
        log.debug("Updating Address: {} with data: {}", id, address)
        val currentAddress = accountRepo.findOne(id)
        return if (currentAddress != null
        ) accountRepo.save(AddressEntity.fromDto(address, currentAddress)).toDto()
        else null
    }

    override fun addAddress(address: CreateAddressDto): AddressDto {
        log.debug("Adding Address: {}", address)
        return accountRepo.save(AddressEntity.fromDto(address)).toDto()
    }

    override fun deleteAddress(id: Long?) {
        log.debug("Deleting Address: {}", id)
        accountRepo.delete(id)
    }
}