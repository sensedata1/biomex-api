package io.biomex.biomex.service

import io.biomex.biomex.service.dto.CreateSubscriptionDto
import io.biomex.biomex.service.dto.SubscriptionDto
import io.biomex.biomex.service.dto.UpdateSubscriptionDto

interface SubscriptionService {

    fun findAllSubscriptions(): List<SubscriptionDto>
    fun findBySubscriptionId(id: Long): SubscriptionDto?
    fun addSubscription(subscription: CreateSubscriptionDto): SubscriptionDto
    fun updateSubscription(subscription: UpdateSubscriptionDto, id: Long): SubscriptionDto?
    fun deleteSubscription(id: Long)

}