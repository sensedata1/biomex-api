package io.biomex.biomex.service

import io.biomex.biomex.InternalUserConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalUserConfig::class])
class UserConfig