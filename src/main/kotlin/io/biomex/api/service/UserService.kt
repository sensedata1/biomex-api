package io.biomex.biomex.service

import io.biomex.biomex.service.dto.CreateUserDto
import io.biomex.biomex.service.dto.UpdateUserDto
import io.biomex.biomex.service.dto.UserDto

interface UserService {

    fun findByUserId(userId: Long): UserDto?

    fun findAllUsers(): List<UserDto>

    fun addUser(user: CreateUserDto): UserDto

    fun updateUser(id: Long?, user: UpdateUserDto): UserDto?

    fun deleteUser(id: Long?)

}