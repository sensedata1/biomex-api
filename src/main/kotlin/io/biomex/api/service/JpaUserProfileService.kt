package io.biomex.biomex.service

import io.biomex.biomex.model.UserProfileEntity
import io.biomex.biomex.repository.UserProfileRepository
import io.biomex.biomex.service.dto.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("userProfileService")
@Transactional
internal class JpaUserProfileService(val userProfileRepository: UserProfileRepository) : UserProfileService {

    val log = LoggerFactory.getLogger("UserProfileService")

    override fun findByUserProfileId(userProfileId: Long): UserProfileDto? {
        log.debug(" User Profile: {}", userProfileId)
        return userProfileRepository.findOne(userProfileId)?.toDto()
    }

    override fun findAllUserProfiles(): List<UserProfileDto> {
        log.debug("Retrieving User Profiles")
        return userProfileRepository.findAll().map { it.toDto() }
    }

    override fun updateUserProfile(id: Long?, user: UpdateUserProfileDto): UserProfileDto? {
        log.debug("Updating User Profile: {} with data: {}", id, user)
        val currentUserProfile = userProfileRepository.findOne(id)
        return if (currentUserProfile != null
        ) userProfileRepository.save(UserProfileEntity.fromDto(user, currentUserProfile)).toDto()
        else null
    }

    override fun addUserProfile(userProfile: CreateUserProfileDto): UserProfileDto {
        log.debug("Adding User Profile: {}", userProfile)
        return userProfileRepository.save(UserProfileEntity.fromDto(userProfile)).toDto()
    }

    override fun deleteUserProfile(id: Long?) {
        log.debug("Deleting User Profile: {}", id)
        userProfileRepository.delete(id)
    }
}