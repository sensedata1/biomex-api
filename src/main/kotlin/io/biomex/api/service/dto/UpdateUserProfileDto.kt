package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateUserProfileDto(
    var firstName: String,
    var lastName: String,
    var otherName: String,
    var nationality: String,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)