package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UserDto(
    var id: Long,
    var name: String,
    var password: String? = null,
    var groups: List<GroupDto>,
    var participant_id: Long,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)