package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateAssetDto(
    @NotEmpty var name: String,
    var description: String? = null,
    @NotEmpty var participant_id: Long,
    @NotEmpty var contract_id: Long,
    @NotEmpty var version: String,
    var biomex_endpoint: String,
    var off_premise_endpoint: String,
    var keywords: String,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)