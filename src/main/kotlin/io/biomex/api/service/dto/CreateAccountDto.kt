package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateAccountDto(
    @NotEmpty var name: String,
    var description: String? = null,
    var bic: String,
    var iban: String,
    var participant_id: Long,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)

