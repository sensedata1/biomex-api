package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class AssetDto(
    var id: Long,
    var name: String,
    var description: String? = null,
    var participant_id: Long,
    var contract_id: Long,
    var version: String,
    var biomex_endpoint: String,
    var off_premise_endpoint: String,
    var keywords: String,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)
