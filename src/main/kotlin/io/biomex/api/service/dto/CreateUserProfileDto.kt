package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class CreateUserProfileDto(
    var firstName: String,
    var lastName: String,
    var otherName: String,
    var nationality: String,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)
