package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class SubscriptionDto(
    val id: Long,
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val term: String,
    val startDate: LocalDateTime,
    val expiryDate: LocalDateTime,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime
)