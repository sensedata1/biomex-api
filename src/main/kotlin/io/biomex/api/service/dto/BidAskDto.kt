package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class BidAskDto(
    val id: Long,
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val bid: Float,
    val ask: Float,
    val startDate: LocalDateTime,
    val expiryDate: LocalDateTime,
    val updatedAt: LocalDateTime,
    val createdAt: LocalDateTime
)