package io.biomex.biomex.service.dto

import io.biomex.biomex.model.AddressType
import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class CreateAddressDto(
    @NotEmpty var line1: String,
    var line2: String? = null,
    var line3: String? = null,
    @NotEmpty var city: String,
    @NotEmpty var state: String,
    @NotEmpty var country: String,
    @NotEmpty var postcode: String,
    @NotEmpty var participant_id: Long,
    var type: AddressType,
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var createdAt: LocalDateTime = LocalDateTime.now()
)

