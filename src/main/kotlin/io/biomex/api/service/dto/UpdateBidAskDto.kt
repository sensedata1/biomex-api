package io.biomex.biomex.service.dto

import org.hibernate.validator.constraints.NotEmpty
import java.time.LocalDateTime

data class UpdateBidAskDto(
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val bid: Float,
    val ask: Float,
    val startDate: LocalDateTime,
    val expiryDate: LocalDateTime,
    val updatedAt: LocalDateTime = LocalDateTime.now()
)