package io.biomex.biomex.service.dto

import io.biomex.biomex.model.ParticipantType
import java.time.LocalDateTime

data class UpdateParticipantDto(
    val name: String,
    val description: String?,
    var type: ParticipantType,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)

