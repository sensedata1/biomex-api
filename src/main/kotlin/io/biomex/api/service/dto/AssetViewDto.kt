package io.biomex.biomex.service.dto

data class AssetViewDto(
    var participantDto: ParticipantDto?,
    var keywords: String
)
