package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateContractDto(
    val type: String,
    val price: String?,
    val participant_id: Long?,
    val asset_id: Long?,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)