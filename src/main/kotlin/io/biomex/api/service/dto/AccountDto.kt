package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class AccountDto(
    var id: Long,
    var name: String,
    var bic: String,
    var iban: String,
    var description: String? = null,
    var participant_id: Long,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)