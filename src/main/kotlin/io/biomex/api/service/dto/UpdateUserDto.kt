package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateUserDto(
    val name: String,
    val password: String?,
    var participant_id: Long,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)