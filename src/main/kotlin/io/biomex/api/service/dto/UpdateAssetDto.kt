package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateAssetDto(
    val name: String,
    val description: String?,
    val participant_id: Long?,
    val contract_id: Long?,
    val version: String,
    var biomex_endpoint: String,
    var off_premise_endpoint: String,
    var keywords: String,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)