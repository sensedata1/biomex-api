package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class CreateSubscriptionDto(
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val term: String,
    val startDate: LocalDateTime,
    val expiryDate: LocalDateTime
)