package io.biomex.biomex.service.dto

import java.time.LocalDateTime
import org.hibernate.validator.constraints.NotEmpty

data class CreateBidAskDto(
    @NotEmpty val contractId: Long,
    val cost: Float,
    val currency: String,
    @NotEmpty val bid: Float,
    @NotEmpty val ask: Float,
    val startDate: LocalDateTime = LocalDateTime.now(),
    val expiryDate: LocalDateTime = LocalDateTime.now(),
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
)