package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UpdateAccountDto(
    val name: String,
    val description: String?,
    var bic: String,
    var iban: String,
    var participant_id: Long,
    var updatedAt: LocalDateTime = LocalDateTime.now()
)