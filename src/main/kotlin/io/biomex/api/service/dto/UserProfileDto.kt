package io.biomex.biomex.service.dto

import java.time.LocalDateTime

data class UserProfileDto(
    var id: Long,
    var firstName: String,
    var lastName: String,
    var otherName: String,
    var nationality: String,
    var updatedAt: LocalDateTime,
    var createdAt: LocalDateTime
)