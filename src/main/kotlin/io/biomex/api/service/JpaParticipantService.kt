package io.biomex.biomex.service

import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.ParticipantDto
import io.biomex.biomex.service.dto.UpdateParticipantDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("participantService")
@Transactional
internal class JpaParticipantService(val participantRepo: ParticipantRepository) : ParticipantService {

    val log = LoggerFactory.getLogger("participantService")

    override fun findByParticipantId(participantId: Long): ParticipantDto? {
        log.debug("Find By Participant Id: {}", participantId)
        return participantRepo.findOne(participantId)?.toDto()
    }

    override fun findAllParticipants(): List<ParticipantDto> {
        log.debug("Find All Participants")
        return participantRepo.findAll().map { it.toDto() }
    }

    override fun updateParticipant(id: Long?, participant: UpdateParticipantDto): ParticipantDto? {
        log.debug("Updating Participant: {} with data: {}", id, participant)
        val currentParticipant = participantRepo.findOne(id)
        return if (currentParticipant != null
        ) participantRepo.save(ParticipantEntity.fromDto(participant, currentParticipant)).toDto()
        else null
    }

    override fun addParticipant(participant: CreateParticipantDto): ParticipantDto {
        log.debug("Adding Participant: {}", participant)
        return participantRepo.save(ParticipantEntity.fromDto(participant)).toDto()
    }

    override fun deleteParticipant(id: Long?) {
        log.debug("Deleting Participant: {}", id)
        participantRepo.delete(id)
    }
}
