package io.biomex.biomex.service

import io.biomex.biomex.service.dto.AssetDto
import io.biomex.biomex.service.dto.CreateAssetDto
import io.biomex.biomex.service.dto.UpdateAssetDto

interface AssetService {

    fun findByAssetId(assetId: Long): AssetDto?

    fun findAllAssets(): List<AssetDto>

    fun addAsset(asset: CreateAssetDto): AssetDto

    fun updateAsset(id: Long?, asset: UpdateAssetDto): AssetDto?

    fun deleteAsset(id: Long?)
}
