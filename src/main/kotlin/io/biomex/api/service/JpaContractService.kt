package io.biomex.biomex.service

import io.biomex.biomex.model.ContractEntity
import io.biomex.biomex.repository.ContractRepository
import io.biomex.biomex.service.dto.ContractDto
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.UpdateContractDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("contractService")
@Transactional
internal class JpaContractService(val contractRepo: ContractRepository) : ContractService {

    val log = LoggerFactory.getLogger("contractService")

    override fun findByContractId(contractId: Long): ContractDto? {
        log.debug("Retrieving Contract: {}", contractId)
        return contractRepo.findOne(contractId)?.toDto()
    }

    override fun findAllContracts(): List<ContractDto> {
        log.debug("Retrieving Contracts")
        return contractRepo.findAll().map { it.toDto() }
    }

    override fun updateContract(id: Long?, contract: UpdateContractDto): ContractDto? {
        log.debug("Updating Contract: {} with data: {}", id, contract)
        val currentContract = contractRepo.findOne(id)
        return if (currentContract != null
        ) contractRepo.save(ContractEntity.fromDto(contract, currentContract)).toDto()
        else null
    }

    override fun addContract(contract: CreateContractDto): ContractDto {
        log.debug("Adding Contract: {}", contract)
        return contractRepo.save(ContractEntity.fromDto(contract)).toDto()
    }

    override fun deleteContract(id: Long?) {
        log.debug("Deleting Contract: {}", id)
        contractRepo.delete(id)
    }
}