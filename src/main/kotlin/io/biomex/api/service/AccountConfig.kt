package io.biomex.biomex.service

import io.biomex.biomex.InternalAccountConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalAccountConfig::class])
class AccountConfig