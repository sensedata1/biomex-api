package io.biomex.biomex.service

import io.biomex.biomex.model.BidAskEntity
import io.biomex.biomex.repository.BidAskRepository
import io.biomex.biomex.service.dto.BidAskDto
import io.biomex.biomex.service.dto.CreateBidAskDto
import io.biomex.biomex.service.dto.UpdateBidAskDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("bidAskService")
@Transactional
internal class JpaBidAskService(val bidAskRepo: BidAskRepository) : BidAskService {

    val log = LoggerFactory.getLogger("bidAskService")

    override fun findByBidAskId(bidAskId: Long): BidAskDto? {
        log.debug("Find By BidAsk Id: {}", bidAskId)
        return bidAskRepo.findOne(bidAskId)?.toDto()
    }

    override fun findAllBidAsks(): List<BidAskDto> {
        log.debug("Find All BidAsks")
        return bidAskRepo.findAll().map { it.toDto() }
    }

    override fun updateBidAsk(id: Long?, bidAsk: UpdateBidAskDto): BidAskDto? {
        log.debug("Updating BidAsk: {} with data: {}", id, bidAsk)
        val currentBidAsk = bidAskRepo.findOne(id)
        return if (currentBidAsk != null
        ) bidAskRepo.save(BidAskEntity.fromDto(bidAsk, currentBidAsk)).toDto()
        else null
    }

    override fun addBidAsk(bidAsk: CreateBidAskDto): BidAskDto {
        log.debug("Adding BidAsk: {}", bidAsk)
        return bidAskRepo.save(BidAskEntity.fromDto(bidAsk)).toDto()
    }

    override fun deleteBidAsk(id: Long?) {
        log.debug("Deleting BidAsk: {}", id)
        bidAskRepo.delete(id)
    }
}
