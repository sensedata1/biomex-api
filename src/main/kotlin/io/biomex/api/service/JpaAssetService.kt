package io.biomex.biomex.service

import io.biomex.biomex.model.AssetEntity
import io.biomex.biomex.repository.AssetRepository
import io.biomex.biomex.service.dto.AssetDto
import io.biomex.biomex.service.dto.CreateAssetDto
import io.biomex.biomex.service.dto.UpdateAssetDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("assetService")
@Transactional
internal class JpaAssetService(val assetRepo: AssetRepository) : AssetService {

    val log = LoggerFactory.getLogger("assetService")

    override fun findByAssetId(assetId: Long): AssetDto? {
        log.debug("Find By Asset Id: {}", assetId)
        return assetRepo.findOne(assetId)?.toDto()
    }

    override fun findAllAssets(): List<AssetDto> {
        log.debug("Find All Assets")
        return assetRepo.findAll().map { it.toDto() }
    }

    override fun updateAsset(id: Long?, asset: UpdateAssetDto): AssetDto? {
        log.debug("Updating Asset: {} with data: {}", id, asset)
        val currentAsset = assetRepo.findOne(id)
        return if (currentAsset != null
        ) assetRepo.save(AssetEntity.fromDto(asset, currentAsset)).toDto()
        else null
    }

    override fun addAsset(asset: CreateAssetDto): AssetDto {
        log.debug("Adding Asset: {}", asset)
        return assetRepo.save(AssetEntity.fromDto(asset)).toDto()
    }

    override fun deleteAsset(id: Long?) {
        log.debug("Deleting Asset: {}", id)
        assetRepo.delete(id)
    }
}
