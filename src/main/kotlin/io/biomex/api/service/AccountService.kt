package io.biomex.biomex.service

import io.biomex.biomex.service.dto.AccountDto
import io.biomex.biomex.service.dto.CreateAccountDto
import io.biomex.biomex.service.dto.UpdateAccountDto

interface AccountService {

    fun findByAccountId(accountId: Long): AccountDto?

    fun findAllAccounts(): List<AccountDto>

    fun addAccount(account: CreateAccountDto): AccountDto

    fun updateAccount(id: Long?, account: UpdateAccountDto): AccountDto?

    fun deleteAccount(id: Long?)

}