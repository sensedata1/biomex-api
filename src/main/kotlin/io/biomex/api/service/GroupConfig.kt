package io.biomex.biomex.service

import io.biomex.biomex.InternalGroupConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalGroupConfig::class])
class GroupConfig
