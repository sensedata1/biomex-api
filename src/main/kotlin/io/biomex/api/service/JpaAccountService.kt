package io.biomex.biomex.service

import io.biomex.biomex.model.AccountEntity
import io.biomex.biomex.repository.AccountRepository
import io.biomex.biomex.service.dto.AccountDto
import io.biomex.biomex.service.dto.CreateAccountDto
import io.biomex.biomex.service.dto.UpdateAccountDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("accountService")
@Transactional
internal class JpaAccountService(val accountRepo: AccountRepository) : AccountService {

    val log = LoggerFactory.getLogger("accountService")

    override fun findByAccountId(accountId: Long): AccountDto? {
        log.debug("Retrieving Account: {}", accountId)
        return accountRepo.findOne(accountId)?.toDto()
    }

    override fun findAllAccounts(): List<AccountDto> {
        log.debug("Retrieving Accounts")
        return accountRepo.findAll().map { it.toDto() }
    }

    override fun updateAccount(id: Long?, account: UpdateAccountDto): AccountDto? {
        log.debug("Updating Account: {} with data: {}", id, account)
        val currentAccount = accountRepo.findOne(id)
        return if (currentAccount != null
        ) accountRepo.save(AccountEntity.fromDto(account, currentAccount)).toDto()
        else null
    }

    override fun addAccount(account: CreateAccountDto): AccountDto {
        log.debug("Adding Account: {}", account)
        return accountRepo.save(AccountEntity.fromDto(account)).toDto()
    }

    override fun deleteAccount(id: Long?) {
        log.debug("Deleting Account: {}", id)
        accountRepo.delete(id)
    }
}