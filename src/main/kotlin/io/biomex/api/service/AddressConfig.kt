package io.biomex.biomex.service

import io.biomex.biomex.InternalAddressConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalAddressConfig::class])
class AddressConfig