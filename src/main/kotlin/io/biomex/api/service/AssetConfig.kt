package io.biomex.biomex.service

import io.biomex.biomex.InternalAssetConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [InternalAssetConfig::class])
class AssetConfig
