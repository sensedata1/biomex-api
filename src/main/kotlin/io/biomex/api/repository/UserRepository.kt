package io.biomex.biomex.repository

import io.biomex.biomex.model.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional(Transactional.TxType.MANDATORY)
internal interface UserRepository : JpaRepository<UserEntity, Long> {

    @Transactional
    fun findByName(name: String): List<UserEntity>

    @Transactional
    fun findById(id: Long?): UserEntity

}