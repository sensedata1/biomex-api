package io.biomex.biomex.repository

import io.biomex.biomex.InternalAddressConfig
import io.biomex.biomex.model.AddressEntity
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
@ComponentScan(basePackageClasses = [InternalAddressConfig::class])
internal interface AddressRepository : JpaRepository<AddressEntity, Long>
