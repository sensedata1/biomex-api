package io.biomex.biomex.repository

import io.biomex.biomex.model.AssetEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional(Transactional.TxType.MANDATORY)
internal interface AssetRepository : JpaRepository<AssetEntity, Long>