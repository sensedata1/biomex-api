package io.biomex.biomex

import io.biomex.biomex.model.GroupEntity
import io.biomex.biomex.repository.GroupRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = arrayOf(GroupRepository::class))
@EntityScan(basePackageClasses = arrayOf(GroupEntity::class))
@EnableTransactionManagement
internal class InternalGroupConfig