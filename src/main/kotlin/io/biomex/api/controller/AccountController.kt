package io.biomex.biomex.controller

import io.biomex.biomex.repository.AccountRepository
import io.biomex.biomex.service.AccountService
import io.biomex.biomex.service.dto.AccountDto
import io.biomex.biomex.service.dto.CreateAccountDto
import io.biomex.biomex.service.dto.UpdateAccountDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/account")
class AccountController {

    @Autowired
    @Qualifier("accountService")
    lateinit var service: AccountService
    @Autowired
    internal lateinit var repository: AccountRepository

    private val log = LoggerFactory.getLogger(AccountController::class.java)

    // Route to findAllAccounts
    @GetMapping("/")
    fun findAllAccounts(): List<AccountDto> {
        log.debug("FindingAll")
        return service.findAllAccounts()
    }

    // Route to findByAccountId
    @GetMapping("/{id}")
    fun findByAccountId(@PathVariable("id") id: Long): AccountDto? {
        log.debug("FindingByAccountId=" + id)
        return service.findByAccountId(id)
    }

    @PostMapping("/")
    fun addAccount(@RequestBody createAccountDto: CreateAccountDto): AccountDto {
        log.debug("Adding account id=" + createAccountDto.toString())
        return service.addAccount(createAccountDto)
    }

    @PutMapping("/{id}")
    fun updateAccount(@RequestBody updateAccountDto: UpdateAccountDto, @PathVariable("id") id: Long): AccountDto? {
        log.debug("Updating account id=" + id)
        return service.updateAccount(id, updateAccountDto)
    }

    @DeleteMapping("/{id}")
    fun deleteAccountId(@PathVariable("id") id: Long) {
        log.debug("Deleting accountId=" + id)
        return service.deleteAccount(id)
    }
}