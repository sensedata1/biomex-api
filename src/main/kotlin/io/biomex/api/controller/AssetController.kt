package io.biomex.biomex.controller

import io.biomex.biomex.service.AssetService
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.UserService
import io.biomex.biomex.service.dto.AssetDto
import io.biomex.biomex.service.dto.AssetViewDto
import io.biomex.biomex.service.dto.CreateAssetDto
import io.biomex.biomex.service.dto.UpdateAssetDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/asset")
class AssetController {

    @Autowired
    @Qualifier("assetService")
    lateinit var service: AssetService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var particiapntService: ParticipantService

    private val log = LoggerFactory.getLogger(AssetController::class.java)

    // Route to findAllAssets
    @GetMapping("/")
    fun findAllAssets(): List<AssetDto> {
        log.debug("FindingAll")
        return service.findAllAssets()
    }

    // Route to findByAssetId
    @GetMapping("/{id}")
    fun findByAssetId(@PathVariable("id") id: Long): AssetDto? {
        log.debug("FindingByAssetId = " + id)
        return service.findByAssetId(id)
    }

    @PostMapping("/")
    fun addAsset(@RequestBody createAssetDto: CreateAssetDto): AssetDto {
        log.debug("Adding asset id=" + createAssetDto.toString())
        return service.addAsset(createAssetDto)
    }

    @PutMapping("/{id}")
    fun updateAsset(@RequestBody updateAssetDto: UpdateAssetDto, @PathVariable("id") id: Long): AssetDto? {
        log.debug("Updating asset id=" + id)
        return service.updateAsset(id, updateAssetDto)
    }

    @DeleteMapping("/{id}")
    fun deleteAssetId(@PathVariable("id") id: Long) {
        log.debug("Deleting assetId=" + id)
        return service.deleteAsset(id)

    }

    // Route to getAssetsView
    @GetMapping("/assetview/{id}")
    fun getAssetViewByUserId(@PathVariable("id") id: Long): AssetViewDto? {
        log.debug("getAssetViewByUserId = " + id)
        val participantId = userService.findByUserId(id)!!.participant_id
        val participantDto = particiapntService.findByParticipantId(participantId)
        return AssetViewDto(participantDto, keywords = "keyword")
    }
}