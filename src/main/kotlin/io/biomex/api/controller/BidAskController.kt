package io.biomex.biomex.controller

import io.biomex.biomex.service.BidAskService
import io.biomex.biomex.service.dto.BidAskDto
import io.biomex.biomex.service.dto.CreateBidAskDto
import io.biomex.biomex.service.dto.UpdateBidAskDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/bidask")
class BidAskController {

    @Autowired
    @Qualifier("bidAskService")
    lateinit var service: BidAskService

    private val log = LoggerFactory.getLogger(BidAskController::class.java)

    // Route to findAllBidAsks
    @GetMapping("/")
    fun findAllBidAsks(): List<BidAskDto> {
        log.debug("FindingAll")
        return service.findAllBidAsks()
    }

    // Route to findByBidAskId
    @GetMapping("/{id}")
    fun findByBidAskId(@PathVariable("id") id: Long): BidAskDto? {
        log.debug("FindingByBidAskId = " + id)
        return service.findByBidAskId(id)
    }

    @PostMapping("/")
    fun addBidAsk(@RequestBody createBidAskDto: CreateBidAskDto): BidAskDto {
        log.debug("Adding BidAsk id=" + createBidAskDto.toString())
        return service.addBidAsk(createBidAskDto)
    }

    @PutMapping("/{id}")
    fun updateBidAsk(@RequestBody updateBidAskDto: UpdateBidAskDto, @PathVariable("id") id: Long): BidAskDto? {
        log.debug("Updating BidAsk id=" + id)
        return service.updateBidAsk(id, updateBidAskDto)
    }

    @DeleteMapping("/{id}")
    fun deleteBidAskId(@PathVariable("id") id: Long) {
        log.debug("Deleting BidAskId=" + id)
        return service.deleteBidAsk(id)

    }
}