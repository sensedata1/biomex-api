package io.biomex.biomex.controller

import io.biomex.biomex.repository.UserRepository
import io.biomex.biomex.service.UserService
import io.biomex.biomex.service.dto.CreateUserDto
import io.biomex.biomex.service.dto.UpdateUserDto
import io.biomex.biomex.service.dto.UserDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/user")
class UserController {

    @Autowired
    @Qualifier("userService")
    lateinit var service: UserService
    @Autowired
    internal lateinit var repository: UserRepository

    private val log = LoggerFactory.getLogger(UserController::class.java)

    // Route to findAllUsers
    @GetMapping("/")
    fun findAllUsers(): List<UserDto> {
        log.debug("FindingAll")
        return service.findAllUsers()
    }

    // Route to findByUserId
    @GetMapping("/{id}")
    fun findByUserId(@PathVariable("id") id: Long): UserDto? {
        log.debug("FindingByUserId=" + id)
        return service.findByUserId(id)
    }

    @PostMapping("/")
    fun addUser(@RequestBody createUserDto: CreateUserDto): UserDto {
        log.debug("Adding User id=" + createUserDto.toString())
        return service.addUser(createUserDto)
    }

    @PutMapping("/{id}")
    fun updateUser(@RequestBody updateUserDto: UpdateUserDto, @PathVariable("id") id: Long): UserDto? {
        log.debug("Updating User id=" + id)
        return service.updateUser(id, updateUserDto)
    }

    @DeleteMapping("/{id}")
    fun deleteUserId(@PathVariable("id") id: Long) {
        log.debug("Deleting UserId=" + id)
        return service.deleteUser(id)
    }
}