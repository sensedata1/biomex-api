package io.biomex.biomex.controller

import io.biomex.biomex.repository.GroupRepository
import io.biomex.biomex.service.GroupService
import io.biomex.biomex.service.dto.CreateGroupDto
import io.biomex.biomex.service.dto.GroupDto
import io.biomex.biomex.service.dto.UpdateGroupDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/group")
class GroupController {

    @Autowired
    @Qualifier("groupService")
    lateinit var service: GroupService
    @Autowired
    internal lateinit var repository: GroupRepository

    private val log = LoggerFactory.getLogger(GroupController::class.java)

    // Route to findAllParticipants
    @GetMapping("/")
    fun findAllGroups(): List<GroupDto> {
        return service.findAllGroups()
    }

    // Route to findByParticipantId
    @GetMapping("/{id}")
    fun findByGroupId(@PathVariable("id") id: Long): GroupDto? {
        log.debug("FindingByGroupId = " + id)
        return service.findByGroupId(id)
    }

    @PostMapping("/")
    fun addGroup(@RequestBody createGroupDto: CreateGroupDto): GroupDto {
        log.debug("Adding Group id=" + createGroupDto.toString())
        return service.addGroup(createGroupDto)
    }

    @PutMapping("/{id}")
    fun updateGroup(@RequestBody updateGroupDto: UpdateGroupDto, @PathVariable("id") id: Long): GroupDto? {
        log.debug("Updating Group id=" + id)
        return service.updateGroup(id, updateGroupDto)
    }

    @DeleteMapping("/{id}")
    fun deleteParticipantId(@PathVariable("id") id: Long) {
        log.debug("Deleting ParticipantId=" + id)
        return service.deleteGroup(id)
    }
}