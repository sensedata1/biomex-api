package io.biomex.biomex.controller

import io.biomex.biomex.repository.ContractRepository
import io.biomex.biomex.service.ContractService
import io.biomex.biomex.service.dto.ContractDto
import io.biomex.biomex.service.dto.CreateContractDto
import io.biomex.biomex.service.dto.UpdateContractDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/contract")
class ContractController {

    @Autowired
    @Qualifier("contractService")
    lateinit var service: ContractService
    @Autowired
    internal lateinit var repository: ContractRepository

    private val log = LoggerFactory.getLogger(ContractController::class.java)

    // Route to findAllContracts
    @GetMapping("/")
    fun findAllContracts(): List<ContractDto> {
        log.debug("FindingAll")
        return service.findAllContracts()
    }

    // Route to findByContractId
    @GetMapping("/{id}")
    fun findByContractId(@PathVariable("id") id: Long): ContractDto? {
        log.debug("FindingByContractId = " + id)
        return service.findByContractId(id)
    }

    @PostMapping("/")
    fun addContract(@RequestBody createContractDto: CreateContractDto): ContractDto {
        log.debug("Adding contract id=" + createContractDto.toString())
        return service.addContract(createContractDto)
    }

    @PutMapping("/{id}")
    fun updateContract(@RequestBody updateContractDto: UpdateContractDto, @PathVariable("id") id: Long): ContractDto? {
        log.debug("Updating contract id=" + id)
        return service.updateContract(id, updateContractDto)
    }

    @DeleteMapping("/{id}")
    fun deleteContractId(@PathVariable("id") id: Long) {
        log.debug("Deleting contractId=" + id)
        return service.deleteContract(id)
    }
}