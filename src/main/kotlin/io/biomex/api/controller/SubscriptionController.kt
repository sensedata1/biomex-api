package io.biomex.biomex.controller

import io.biomex.biomex.service.SubscriptionService
import io.biomex.biomex.service.dto.CreateSubscriptionDto
import io.biomex.biomex.service.dto.SubscriptionDto
import io.biomex.biomex.service.dto.UpdateSubscriptionDto
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("v1/subscription")
class SubscriptionController(private val service: SubscriptionService) {

    private val log = LoggerFactory.getLogger(AccountController::class.java)

    @GetMapping()
    fun findAllSubscriptions(): List<SubscriptionDto> {
        return service.findAllSubscriptions()
    }

    @GetMapping("/{id}")
    fun findBySubscriptionId(@PathVariable("id") id: Long): SubscriptionDto? {
        return service.findBySubscriptionId(id)
    }

    @PostMapping()
    fun addSubscription(@RequestBody createSubscriptionDto: CreateSubscriptionDto): SubscriptionDto {
        return service.addSubscription(createSubscriptionDto)
    }

    @PutMapping("/{id}")
    fun updateSubscription(@RequestBody updateSubscriptionDto: UpdateSubscriptionDto, @PathVariable("id") id: Long): SubscriptionDto? {
        return service.updateSubscription(updateSubscriptionDto, id)
    }

    @DeleteMapping("/{id}")
    fun deleteSubscription(@PathVariable("id") id: Long) {
        return service.deleteSubscription(id)
    }
}
