package io.biomex.biomex.controller

import io.biomex.biomex.repository.AddressRepository
import io.biomex.biomex.service.AddressService
import io.biomex.biomex.service.dto.AddressDto
import io.biomex.biomex.service.dto.CreateAddressDto
import io.biomex.biomex.service.dto.UpdateAddressDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/address")
class AddressController {

    @Autowired
    @Qualifier("addressService")
    lateinit var service: AddressService
    @Autowired
    internal lateinit var repository: AddressRepository

    private val log = LoggerFactory.getLogger(AddressController::class.java)

    // Route to findAllAddresses
    @GetMapping("/")
    fun findAllAddresses(): List<AddressDto> {
        log.debug("FindingAll")
        return service.findAllAddresses()
    }

    // Route to findByAddressId
    @GetMapping("/{id}")
    fun findByAddressId(@PathVariable("id") id: Long): AddressDto? {
        log.debug("FindingByAddresstId=" + id)
        return service.findByAddressId(id)
    }

    @PostMapping("/")
    fun addAddress(@RequestBody createAddressDto: CreateAddressDto): AddressDto {
        log.debug("Adding address id=" + createAddressDto.toString())
        return service.addAddress(createAddressDto)
    }

    @PutMapping("/{id}")
    fun updateAddress(@RequestBody updateAddressDto: UpdateAddressDto, @PathVariable("id") id: Long): AddressDto? {
        log.debug("Updating address id=" + id)
        return service.updateAddress(id, updateAddressDto)
    }

    @DeleteMapping("/{id}")
    fun deleteAddressId(@PathVariable("id") id: Long) {
        log.debug("Deleting addressId=" + id)
        return service.deleteAddress(id)
    }
}