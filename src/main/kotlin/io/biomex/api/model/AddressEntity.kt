package io.biomex.biomex.model

import io.biomex.biomex.service.dto.AddressDto
import io.biomex.biomex.service.dto.CreateAddressDto
import io.biomex.biomex.service.dto.UpdateAddressDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "address")
internal data class AddressEntity(

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val line1: String,
    val line2: String? = null,
    val line3: String? = null,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    @JoinColumn
    val participant_id: Long? = null,
    @Enumerated
    @Column(columnDefinition = "smallint")
    private val type: AddressType? = null,
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        line1 = "",
        line2 = "",
        line3 = "",
        city = "",
        state = "",
        country = "",
        postcode = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): AddressDto = AddressDto(
        id = this.id!!,
        line1 = this.line1,
        line2 = this.line2,
        line3 = this.line3,
        city = this.city,
        state = this.state,
        country = this.country,
        postcode = this.postcode,
        participant_id = this.participant_id!!,
        type = this.type!!,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: AddressDto) = AddressDto(
            id = dto.id,
            line1 = dto.line1,
            line2 = dto.line2,
            line3 = dto.line3,
            city = dto.city,
            state = dto.state,
            country = dto.country,
            postcode = dto.postcode,
            participant_id = dto.participant_id,
            type = dto.type,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateAddressDto) = AddressEntity(
            line1 = dto.line1,
            line2 = dto.line2,
            line3 = dto.line3,
            city = dto.city,
            state = dto.state,
            country = dto.country,
            postcode = dto.postcode,
            type = dto.type,
            participant_id = dto.participant_id
        )

        fun fromDto(dto: UpdateAddressDto, defaultAddress: AddressEntity) = AddressEntity(
            id = defaultAddress.id!!,
            line1 = dto.line1,
            line2 = dto.line2,
            line3 = dto.line3,
            city = dto.city,
            state = dto.state,
            country = dto.country,
            postcode = dto.postcode,
            participant_id = dto.participant_id,
            type = dto.type,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultAddress.createdAt
        )
    }
}