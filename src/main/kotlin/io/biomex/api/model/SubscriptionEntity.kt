package io.biomex.biomex.model

import io.biomex.biomex.service.dto.CreateSubscriptionDto
import io.biomex.biomex.service.dto.SubscriptionDto
import io.biomex.biomex.service.dto.UpdateSubscriptionDto
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "subscription")
internal data class SubscriptionEntity(
    @Id @GeneratedValue val id: Long = -1,
    val contractId: Long,
    val cost: Float,
    val currency: String,
    val term: String,
    val startDate: LocalDateTime,
    val expiryDate: LocalDateTime,
    val createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
) {

    fun toDto(): SubscriptionDto = SubscriptionDto(
        id = this.id,
        contractId = this.contractId,
        cost = this.cost,
        currency = this.currency,
        term = this.term,
        startDate = this.startDate,
        expiryDate = this.expiryDate,
        createdAt = this.createdAt,
        updatedAt = this.updatedAt
    )

    companion object {
        fun fromDto(dto: CreateSubscriptionDto) = SubscriptionEntity(
            contractId = dto.contractId,
            cost = dto.cost,
            currency = dto.currency,
            term = dto.term,
            startDate = dto.startDate,
            expiryDate = dto.expiryDate
        )
        fun fromDto(updatedSubscription: UpdateSubscriptionDto, currentSubscription: SubscriptionEntity) = SubscriptionEntity(
            id = currentSubscription.id,
            createdAt = currentSubscription.createdAt,
            contractId = updatedSubscription.contractId,
            cost = updatedSubscription.cost,
            currency = updatedSubscription.currency,
            term = updatedSubscription.term,
            startDate = updatedSubscription.startDate,
            expiryDate = updatedSubscription.expiryDate
        )
    }
}
