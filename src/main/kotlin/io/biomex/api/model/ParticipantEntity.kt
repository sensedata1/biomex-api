package io.biomex.biomex.model

import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.ParticipantDto
import io.biomex.biomex.service.dto.UpdateParticipantDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "participant")
internal data class ParticipantEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val name: String,
    val description: String? = null,
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val assets: List<AssetEntity> = emptyList(),
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val contracts: List<ContractEntity> = emptyList(),
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val addresses: List<AddressEntity> = emptyList(),
    @Enumerated
    @Column(columnDefinition = "smallint")
    private val type: ParticipantType? = null,
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val accounts: List<AccountEntity> = emptyList(),
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val users: List<UserEntity> = emptyList(),
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        name = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): ParticipantDto = ParticipantDto(
        id = this.id!!,
        name = this.name,
        description = this.description,
        assets = this.assets.map { it.toDto() },
        contracts = this.contracts.map { it.toDto() },
        addresses = this.addresses.map { it.toDto() },
        type = this.type!!,
        accounts = this.accounts.map { it.toDto() },
        users = this.users.map { it.toDto() },
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: ParticipantDto) = ParticipantEntity(
            id = dto.id,
            name = dto.name,
            description = dto.description,
            type = dto.type,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateParticipantDto) = ParticipantEntity(
            name = dto.name,
            type = dto.type,
            description = dto.description
        )

        fun fromDto(dto: UpdateParticipantDto, defaultParticipant: ParticipantEntity) = ParticipantEntity(
            id = defaultParticipant.id!!,
            name = dto.name,
            description = dto.description ?: defaultParticipant.description,
            type = dto.type,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultParticipant.createdAt
        )
    }
}