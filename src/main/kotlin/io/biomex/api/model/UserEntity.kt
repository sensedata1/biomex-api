package io.biomex.biomex.model

import io.biomex.biomex.service.dto.CreateUserDto
import io.biomex.biomex.service.dto.UpdateUserDto
import io.biomex.biomex.service.dto.UserDto
import java.time.LocalDateTime
import javax.persistence.*


@Entity
@Table(name = "Users")
internal data class UserEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val name: String,
    val password: String? = null,
    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "Users_Groups",
        joinColumns = [(JoinColumn(name = "user_id", referencedColumnName = "id"))],
        inverseJoinColumns = [(JoinColumn(name = "group_id", referencedColumnName = "id"))]
    )
    private val groups: Set<GroupEntity> = emptySet(),
    @JoinColumn
    val participant_id: Long? = null,
    @OneToMany(
        mappedBy = "participant_id", cascade = [CascadeType.ALL], orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private val assets: List<AssetEntity> = emptyList(),

    @OneToOne(cascade = arrayOf(CascadeType.ALL))
    val userProfile: UserProfileEntity ?= null,

    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        name = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): UserDto = UserDto(
        id = this.id!!,
        name = this.name,
        password = this.password,
        groups = this.groups.map { it.toDto() },
        participant_id = this.participant_id!!,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: UserDto) = UserEntity(
            id = dto.id,
            name = dto.name,
            password = dto.password,
            participant_id = dto.participant_id,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateUserDto) = UserEntity(
            name = dto.name,
            password = dto.password,
            participant_id = dto.participant_id
        )

        fun fromDto(dto: UpdateUserDto, defaultUser: UserEntity) = UserEntity(
            id = defaultUser.id!!,
            name = dto.name,
            password = dto.password ?: defaultUser.password,
            participant_id = dto.participant_id,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultUser.createdAt
        )
    }
}