package io.biomex.biomex.model

import io.biomex.biomex.service.dto.AccountDto
import io.biomex.biomex.service.dto.CreateAccountDto
import io.biomex.biomex.service.dto.UpdateAccountDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "account")
internal data class AccountEntity(

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val name: String,
    val bic: String,
    val iban: String,
    val description: String? = null,
    @JoinColumn
    val participant_id: Long? = null,
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        name = "",
        bic = "",
        iban = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): AccountDto = AccountDto(
        id = this.id!!,
        name = this.name,
        bic = this.bic,
        iban = this.iban,
        description = this.description,
        participant_id = this.participant_id!!,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: AccountDto) = AccountEntity(
            id = dto.id,
            name = dto.name,
            bic = dto.bic,
            iban = dto.iban,
            description = dto.description,
            participant_id = dto.participant_id,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateAccountDto) = AccountEntity(
            name = dto.name,
            bic = dto.bic,
            iban = dto.iban,
            participant_id = dto.participant_id,
            description = dto.description
        )

        fun fromDto(dto: UpdateAccountDto, defaultAccount: AccountEntity) = AccountEntity(
            id = defaultAccount.id!!,
            name = dto.name,
            bic = dto.bic,
            iban = dto.iban,
            description = dto.description ?: defaultAccount.description,
            participant_id = dto.participant_id,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultAccount.createdAt
        )
    }
}