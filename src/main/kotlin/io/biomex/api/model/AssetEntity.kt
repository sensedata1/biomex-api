package io.biomex.biomex.model

import io.biomex.biomex.service.dto.AssetDto
import io.biomex.biomex.service.dto.CreateAssetDto
import io.biomex.biomex.service.dto.UpdateAssetDto
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "asset")
internal data class AssetEntity(
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id val id: Long? = null,
    val name: String,
    val description: String? = null,
    @JoinColumn
    val participant_id: Long? = null,
    @JoinColumn
    val contract_id: Long? = null,
    val version: String,
    var biomex_endpoint: String,
    var off_premise_endpoint: String,
    var keywords: String,
    @Column(columnDefinition = "smallint")
    private val type: AddressType? = null,
    val updatedAt: LocalDateTime = LocalDateTime.now(),
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    @Suppress("unused")
    private constructor() : this(
        name = "",
        version = "",
        biomex_endpoint = "",
        off_premise_endpoint = "",
        keywords = "",
        updatedAt = LocalDateTime.MIN
    )

    fun toDto(): AssetDto = AssetDto(
        id = this.id!!,
        name = this.name,
        description = this.description,
        participant_id = this.participant_id!!,
        contract_id = this.contract_id!!,
        version = this.version,
        biomex_endpoint = this.biomex_endpoint,
        off_premise_endpoint = this.off_premise_endpoint,
        keywords = this.keywords,
        updatedAt = this.updatedAt,
        createdAt = this.createdAt
    )

    companion object {

        fun fromDto(dto: AssetDto) = AssetEntity(
            id = dto.id,
            name = dto.name,
            description = dto.description,
            participant_id = dto.participant_id,
            contract_id = dto.contract_id,
            version = dto.version,
            biomex_endpoint = dto.biomex_endpoint,
            off_premise_endpoint = dto.off_premise_endpoint,
            keywords = dto.keywords,
            updatedAt = dto.updatedAt,
            createdAt = dto.createdAt
        )

        fun fromDto(dto: CreateAssetDto) = AssetEntity(
            name = dto.name,
            description = dto.description,
            participant_id = dto.participant_id,
            contract_id = dto.contract_id,
            version = dto.version,
            biomex_endpoint = dto.biomex_endpoint,
            off_premise_endpoint = dto.off_premise_endpoint,
            keywords = dto.keywords
        )

        fun fromDto(dto: UpdateAssetDto, defaultAsset: AssetEntity) = AssetEntity(
            id = defaultAsset.id!!,
            name = dto.name,
            description = dto.description ?: defaultAsset.description,
            participant_id = dto.participant_id,
            contract_id = dto.contract_id,
            version = dto.version,
            biomex_endpoint = dto.biomex_endpoint,
            off_premise_endpoint = dto.off_premise_endpoint,
            keywords = dto.keywords,
            updatedAt = LocalDateTime.now(),
            createdAt = defaultAsset.createdAt
        )
    }
}