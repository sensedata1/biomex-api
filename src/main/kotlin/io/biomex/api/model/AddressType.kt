package io.biomex.biomex.model

enum class AddressType {
    RESIDENTIAL,
    DELIVERY,
    BILLING,
    WORK
}