package io.biomex.biomex

import io.biomex.biomex.model.ParticipantEntity
import io.biomex.biomex.model.ParticipantType
import io.biomex.biomex.repository.ParticipantRepository
import io.biomex.biomex.service.ParticipantConfig
import io.biomex.biomex.service.ParticipantService
import io.biomex.biomex.service.dto.CreateParticipantDto
import io.biomex.biomex.service.dto.UpdateParticipantDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [(JpaParticipantServiceTest.Config::class), (ParticipantConfig::class)])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA Participant Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaParticipantServiceTest {

    class Config

    @Autowired
    lateinit var participantRepo: ParticipantRepository

    @Autowired
    lateinit var service: ParticipantService

    @get:Rule
    var softly = JUnitSoftAssertions()

    var firstParticipantId: Long = 0
    @BeforeAll
    fun setUp() {
        participantRepo.deleteAll()
        val (id) = service.addParticipant(CreateParticipantDto(
                        "participant1",
                        "description1",
                         ParticipantType.PRIVATE_LIMITED_COMPANY)
        )
        firstParticipantId = id
    }

    @AfterAll
    fun tearDown() {
        participantRepo.deleteAll()
    }

    @Test
    fun `'findByParticipantId' should return null if participant Id doesn't exist`() {
        assertThat(service.findByParticipantId(0)).isNull()
    }

    var secondParticipantId: Long = 0
    @Test
    fun `'addParticipant' should return created entity`() {
        val (id, name, description, participant_id_asset, participant_id_contract,
                participant_id_address, participant_type, participant_id_accounts,
                participant_id_users, updatedAt, createdAt) =
                service.addParticipant(
                    CreateParticipantDto(
                        "participant2",
                        "description2",
                        ParticipantType.PRIVATE_LIMITED_COMPANY
                    )
                )
        secondParticipantId = id
        softly.assertThat(name).isEqualTo("participant2")
        softly.assertThat(description).isEqualTo("description2")
        softly.assertThat(participant_id_asset).isNullOrEmpty()
        softly.assertThat(participant_id_contract).isNullOrEmpty()
        softly.assertThat(participant_id_address).isNullOrEmpty()
        softly.assertThat(participant_type).isEqualByComparingTo(ParticipantType.PRIVATE_LIMITED_COMPANY)
        softly.assertThat(participant_id_accounts).isNullOrEmpty()
        softly.assertThat(participant_id_users).isNullOrEmpty()
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByParticipantId' should map existing entity from repository`() {
        val result = service.findByParticipantId(firstParticipantId)
        softly.assertThat(result?.id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.name).isEqualTo("participant1")
        softly.assertThat(result?.description).isEqualTo("description1")
    }

    @Test
    fun `'findAllParticipants' should map entity from repository`() {
        val result = service.findAllParticipants()
        softly.assertThat(result).hasSize(2)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateParticipant' should update existing values`() {
        val result = service.updateParticipant(
            firstParticipantId,
            UpdateParticipantDto(
                "new name",
                "new description",
                ParticipantType.PRIVATE_LIMITED_COMPANY,
                LocalDateTime.now()
            )
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(firstParticipantId)
        softly.assertThat(result?.name).isEqualTo("new name")
        softly.assertThat(result?.description).isEqualTo("new description")
        softly.assertThat(result?.type).isEqualTo(ParticipantType.PRIVATE_LIMITED_COMPANY)
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}
