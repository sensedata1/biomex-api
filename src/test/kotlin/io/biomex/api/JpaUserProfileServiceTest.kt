package io.biomex.biomex

import io.biomex.biomex.repository.UserProfileRepository
import io.biomex.biomex.service.UserProfileConfig
import io.biomex.biomex.service.UserProfileService
import io.biomex.biomex.service.dto.CreateUserProfileDto
import io.biomex.biomex.service.dto.UpdateUserProfileDto
import org.assertj.core.api.Assertions
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [(JpaUserProfileServiceTest.Config::class), (UserProfileConfig::class)])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA User Profile Service Tests")
@SpringJUnitConfig
internal class JpaUserProfileServiceTest {

    class Config

    @Autowired
    lateinit var userProfileRepo: UserProfileRepository

    @Autowired
    lateinit var service: UserProfileService

    @get:Rule
    var softly = JUnitSoftAssertions()

    @BeforeAll
    fun setUp() {
        userProfileRepo.deleteAll()
        service.addUserProfile(CreateUserProfileDto("firstName", "lastName", "otherName", "nationality"))
    }

    @Test
    fun `'findByUserProfileId' should return null if User Id doesn't exist`() {
        Assertions.assertThat(service.findByUserProfileId(0)).isNull()
    }

    @Test
    fun `'addUserProfile' should return created entity`() {
        val (id, firstName, lastName, otherName, nationality, updatedAt, createdAt) = service.addUserProfile(
            CreateUserProfileDto(
                "firstName",
                "lastName",
                "otherName",
                "nationality"
            )
        )
        softly.assertThat(id).isEqualTo(2)
        softly.assertThat(firstName).isEqualTo("firstName")
        softly.assertThat(lastName).isEqualTo("lastName")
        softly.assertThat(otherName).isEqualTo("otherName")
        softly.assertThat(nationality).isEqualTo("nationality")
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByUserProfileId' should map existing entity from repository`() {
        val result = service.findByUserProfileId(1)
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.firstName).isEqualTo("firstName")
        softly.assertThat(result?.lastName).isEqualTo("lastName")
        softly.assertThat(result?.otherName).isEqualTo("otherName")
        softly.assertThat(result?.nationality).isEqualTo("nationality")
    }

    @Test
    fun `'findAllUserProfiles' should map entity from repository`() {
        val result = service.findAllUserProfiles()

        softly.assertThat(result).hasSize(1)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateUserProfile' should update existing values`() {
        val result = service.updateUserProfile(
            1, UpdateUserProfileDto(
                "updatedFirstName",
                "updatedSecondName", "newOtherName", "newNationality", LocalDateTime.now()
            )
        )
        softly.assertThat(result).isNotNull
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.firstName).isEqualTo("updatedFirstName")
        softly.assertThat(result?.lastName).isEqualTo("updatedLastName")
        softly.assertThat(result?.otherName).isEqualTo("updatedOtherName")
        softly.assertThat(result?.nationality).isEqualTo("updatedNationality")
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}