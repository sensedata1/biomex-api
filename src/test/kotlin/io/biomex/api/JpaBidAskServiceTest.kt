package io.biomex.biomex

import io.biomex.biomex.repository.BidAskRepository
import io.biomex.biomex.service.BidAskConfig
import io.biomex.biomex.service.BidAskService
import io.biomex.biomex.service.dto.CreateBidAskDto
import io.biomex.biomex.service.dto.UpdateBidAskDto
import org.assertj.core.api.Assertions
import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ActiveProfiles("test")
@ContextConfiguration(classes = [JpaBidAskServiceTest.Config::class, BidAskConfig::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@DisplayName("JPA BidAsk Service Tests")
@ExtendWith(SpringExtension::class)
internal class JpaBidAskServiceTest {

    class Config

    @Autowired
    lateinit var bidAskRepo: BidAskRepository

    @Autowired
    lateinit var service: BidAskService

    @get:Rule
    var softly = JUnitSoftAssertions()

    @BeforeAll
    fun setUp() {
        bidAskRepo.deleteAll()
        service.addBidAsk(
            CreateBidAskDto(
                1,
                100F,
                "ISO 978",
                10F,
                10F,
                LocalDateTime.now()
            )
        )
    }

    @Test
    fun `'findByBidAskId' should return null if BidAsk for BidAsk Id doesn't exist`() {
        Assertions.assertThat(service.findByBidAskId(0)).isNull()
    }

    @Test
    fun `'addBidAsk' should return created entity`() {
        val (id, contract_id, cost, currency, bid, ask, startDate, expireDate, updatedAt, createdAt) = service.addBidAsk(
            CreateBidAskDto(
                1,
                100F,
                "ISO 978",
                10F,
                10F,
                LocalDateTime.now()
            )
        )

        softly.assertThat(id).isEqualTo(1)
        softly.assertThat(contract_id).isEqualTo(1)
        softly.assertThat(cost).isEqualTo(100F)
        softly.assertThat(currency).isEqualTo("ISO 978")
        softly.assertThat(bid).isEqualTo(10F)
        softly.assertThat(ask).isEqualTo(10F)
        softly.assertThat(startDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(expireDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }

    @Test
    fun `'findByBidAskId' should map existing entity from repository`() {
        val result = service.findByBidAskId(1)
        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.contractId).isEqualTo(1)
        softly.assertThat(result?.cost).isEqualTo(100F)
        softly.assertThat(result?.currency).isEqualTo("ISO 978")
        softly.assertThat(result?.bid).isEqualTo(10F)
        softly.assertThat(result?.ask).isEqualTo(10F)
        softly.assertThat(result?.startDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.expiryDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())

    }

    @Test
    fun `'findAllBidAsks' should map entity from repository`() {
        val result = service.findAllBidAsks()

        softly.assertThat(result).hasSize(1)
        result.forEach {
            softly.assertThat(it.id).isNotNull
        }
    }

    @Test
    fun `'updateBidAsk' should update existing values`() {
        val result = service.updateBidAsk(
            1, UpdateBidAskDto(
                1,
                100F,
                "ISO 978",
                10F,
                10F,
                LocalDateTime.now(),
                LocalDateTime.now()
            )
        )

        softly.assertThat(result?.id).isEqualTo(1)
        softly.assertThat(result?.contractId).isEqualTo(1)
        softly.assertThat(result?.cost).isEqualTo(100F)
        softly.assertThat(result?.currency).isEqualTo("ISO 978")
        softly.assertThat(result?.bid).isEqualTo(10F)
        softly.assertThat(result?.ask).isEqualTo(10F)
        softly.assertThat(result?.startDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.expiryDate).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.updatedAt).isBeforeOrEqualTo(LocalDateTime.now())
        softly.assertThat(result?.createdAt).isBeforeOrEqualTo(LocalDateTime.now())
    }
}
